/***********************************************************************************************************************
 *
 * Copyright (C) 2013 by the Stratosphere project (http://stratosphere.eu)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 **********************************************************************************************************************/
package eu.stratosphere.tutorial.task2;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import eu.stratosphere.api.java.record.functions.MapFunction;
import eu.stratosphere.tutorial.util.Util;
import eu.stratosphere.types.IntValue;
import eu.stratosphere.types.Record;
import eu.stratosphere.types.StringValue;
import eu.stratosphere.util.Collector;


/**
 * This mapper computes the term frequency for each term in a document.
 * <p>
 * The term frequency of a term in a document is the number of times the term occurs in the respective document. If a
 * document contains a term three times, the term has a term frequency of 3 (for this document).
 * <p>
 * Example:
 * 
 * <pre>
 * Document 1: "Big Big Big Data"
 * Document 2: "Hello Big Data"
 * </pre>
 * 
 * The term frequency of "Big" in document 1 is 3 and 1 in document 2.
 * <p>
 * The map method will be called independently for each document.
 */
public class TermFrequencyMapper extends MapFunction {

	// ----------------------------------------------------------------------------------------------------------------

	/**
	 * Splits the document into terms and emits a PactRecord (docId, term, tf) for each term of the document.
	 * <p>
	 * Each input document has the format "docId, document contents".
	 */
	@Override
	public void map(Record record, Collector<Record> collector) {
		// each call of the map() function gets one line (e.g. a document).
		String document = record.getField(0, StringValue.class).toString();
		// The schema of the input file looks like this: docid, document contents
		int indexComma = document.indexOf(',');
		int docId = Integer.parseInt(document.substring(0, indexComma));
		document = document.substring(indexComma + 1);

		// Use a HashMap to identify the frequency of each word in the document
		HashMap<String, Integer> freqWords = new HashMap<String, Integer>();

		/* First, split the input line so that only alphanumerical words (\\W)
		 * are accepted. => Use toLowerCase to easily avoid duplicated words
		 */
		String[] array = document.replaceAll("\\W", " ").toLowerCase().split(" ");

		// First, list all words with the count
		for (String word : array) {
			/* Then, check if the individual terms are in the HashSet
			 *  (Util.STOP_WORDS) of stopwords
			 */
			if (! Util.STOP_WORDS.contains(word)) {
				// tf will be null if freqWords doesn't contain word
				Integer tf = freqWords.get(word);
				int count = tf != null ? tf + 1 : 1;
				freqWords.put(word, count);
			}
		}

		// ok, we have all words and the count, add that to the collector.
		Set<Entry<String, Integer>> wordsSet = freqWords.entrySet();
		for (Entry<String, Integer> entry : wordsSet) {
			// This time, the output tuples shall look like this (docid, word, frequency).
			Record wordRecord = new Record(3);
			wordRecord.setField(0, new IntValue(docId));
			wordRecord.setField(1, new StringValue(entry.getKey()));
			wordRecord.setField(2, new IntValue(entry.getValue()));
			collector.collect(wordRecord);
		}
	}
}